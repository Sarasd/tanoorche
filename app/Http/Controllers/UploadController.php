<?php

namespace App\Http\Controllers;

use App\Services\Image\ImageService;

class UploadController extends AppBaseController
{
    public function upload()
    {
        /** @var ImageService $uploadService */
        $uploadService = app(ImageService::class);
        $output_image = array();
        $data = collect(request()->all());
        if (isset($data['file'])) {
            if (isset($data['resize'])) {
                if (is_array($data['resize'])) {
                    $output_image[] = $uploadService->uploadMultiSize($data);
                } else {
                    $output_image[] = $uploadService->uploadAndResize($data);
                }
            } else {
                $output_image[] = $uploadService->upload($data['file']);
            }
        }
        $data['path'] = $output_image;
        return $this->sendResponse($data['path'], "Image Upload SuccessFully");
    }
}
